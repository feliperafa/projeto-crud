const express = require('express');
const bodyParser = require('body-parser'); 
const app = express();


app.use(bodyParser.json())
//localhost:3000/api/tarefas
app.use('/api', require("./src/routes"));
 
 
app.listen(3000);
 